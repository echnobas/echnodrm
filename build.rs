fn main() -> Result<(), Box<dyn std::error::Error>> {
    if cfg!(target_os = "windows") {
        let mut res = winres::WindowsResource::new();
        res
            .append_rc_content(r#"INSTALLER_BIN RCDATA "C:\Users\isaac\Documents\Rust\echnodrm\Cargo.toml""#)
            // manually set version 1.0.0.0
            .set_version_info(winres::VersionInfo::PRODUCTVERSION, 0x0001000000000000);
        res.compile()?;
        println!("cargo:warning=done");
    }
    Ok(())
}