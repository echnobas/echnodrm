use std::{arch::asm, mem};
use winapi::{
    ctypes::{c_long, c_ulong},
    shared::minwindef::FALSE,
    um::{
        debugapi::{CheckRemoteDebuggerPresent, IsDebuggerPresent},
        errhandlingapi::{
            AddVectoredExceptionHandler, GetLastError, RemoveVectoredExceptionHandler,
        },
        handleapi::CloseHandle,
        libloaderapi::{FreeLibrary, GetProcAddress, LoadLibraryA, FindResourceExW, FindResourceW},
        processthreadsapi::{GetCurrentProcess, OpenProcess},
        winnt::{EXCEPTION_POINTERS, LONG, PROCESS_ALL_ACCESS}, winuser::{MessageBoxA, MB_OK},
    },
    vc::excpt::EXCEPTION_CONTINUE_EXECUTION,
};

#[inline(always)]
fn debugger_present() -> bool {
    unsafe { IsDebuggerPresent() != 0 }
}

#[inline(always)]
fn remote_debugger_present() -> bool {
    let mut present = 0;
    unsafe {
        CheckRemoteDebuggerPresent(GetCurrentProcess(), &mut present as *mut _) != 0 && present != 0
    }
}

#[inline(always)]
fn debugger_present_filter() -> bool {
    unsafe {
        let debugged: i32;

        unsafe extern "system" fn handler(info: *mut EXCEPTION_POINTERS) -> LONG {
            let ctx = (*info).ContextRecord;
            (*ctx).Rip += 3;
            return EXCEPTION_CONTINUE_EXECUTION;
        }
        let handler = AddVectoredExceptionHandler(1, Some(handler));
        asm!(
            // Initialise here because LLVM would optimise a previous initialisation UNLESS inout was used
            /*
                Really strange behaviour,
                I guess inout guaranteeing that the same register being used for input and output
                guarantees it WILL be initialised with 1 (asm is volatile by default)
            */
            "mov {0:r}, 1",
            "int 3", // 0xCC
            "jmp 2f", // 0xEB 0x07
            "mov {0:r}, 0", // 0x48 0xC7 0xC0 0x00 0x00 0x00 0x00
            "2:",
            "nop", // 0x90
            out(reg) debugged,
        );
        RemoveVectoredExceptionHandler(handler);
        debugged == 1
    }
}

#[allow(non_snake_case)]
#[inline(always)]
fn debugger_present_privileges() -> bool {
    unsafe {
        let ntdll = LoadLibraryA("ntdll.dll\0".as_ptr() as *const _);
        if ntdll.is_null() {
            return false;
        }
        let CsrGetProcessId = GetProcAddress(ntdll, "CsrGetProcessId\0".as_ptr() as *const _);
        if CsrGetProcessId.is_null() {
            return false;
        }
        let CsrGetProcessId =
            mem::transmute::<_, unsafe extern "system" fn() -> u32>(CsrGetProcessId);
        let csr = OpenProcess(PROCESS_ALL_ACCESS, FALSE, CsrGetProcessId());
        if !csr.is_null() {
            CloseHandle(csr);
            FreeLibrary(ntdll);
            true
        } else {
            FreeLibrary(ntdll);
            false
        }
    }
}

fn die() {
    unsafe {
        let ntdll = LoadLibraryA("ntdll.dll\0".as_ptr() as _);
        if ntdll.is_null() {
            panic!("LoadLibraryA failed ({}): (null)", GetLastError());
        }

        let rtl_adjust_privilege = GetProcAddress(ntdll, "RtlAdjustPrivilege\0".as_ptr() as _);
        if rtl_adjust_privilege.is_null() {
            panic!("GetProcAddress failed ({}): (null)", GetLastError());
        }

        // NtRaiseHardError
        let nt_raise_hard_error = GetProcAddress(ntdll, "NtRaiseHardError\0".as_ptr() as _);
        if nt_raise_hard_error.is_null() {
            panic!("GetProcAddress failed ({}): (null)", GetLastError());
        }

        let mut b_enabled = 0u8;
        let mut u_resp: c_ulong = 0;

        // typedef NTSTATUS(NTAPI *pdef_RtlAdjustPrivilege)(ULONG Privilege, BOOLEAN Enable, BOOLEAN CurrentThread, PBOOLEAN Enabled);
        core::mem::transmute::<_, unsafe extern "system" fn(c_ulong, u8, u8, *mut u8) -> c_long>(
            rtl_adjust_privilege,
        )(19, 1, 0, (&mut b_enabled) as *mut _);

        // typedef NTSTATUS(NTAPI *pdef_NtRaiseHardError)(NTSTATUS ErrorStatus, ULONG NumberOfParameters, ULONG UnicodeStringParameterMask OPTIONAL, PULONG_PTR Parameters, ULONG ResponseOption, PULONG Response);
        #[allow(overflowing_literals)]
        core::mem::transmute::<
            _,
            unsafe extern "system" fn(
                c_long,
                c_ulong,
                c_ulong,
                *mut usize,
                c_ulong,
                *mut c_ulong,
            ) -> c_long,
        >(nt_raise_hard_error)(
            0xC00002B4,
            0,
            0,
            core::ptr::null_mut(),
            6,
            (&mut u_resp) as *mut _,
        );
    }
}

fn main() {
    println!("{} {} {} {}", debugger_present(), debugger_present_filter(), remote_debugger_present(), debugger_present_privileges());
    loop {
    //     if [
    //         debugger_present(),
    //         debugger_present_filter(),
    //         remote_debugger_present(),
    //         debugger_present_privileges(),
    //     ]
    //     .into_iter()
    //     .any(|b| b)
    //     {
    //         std::thread::spawn(|| {
    //             std::thread::sleep_ms(5000);
    //             // die();
    //             std::process::exit(1);
    //         });
    //         unsafe {
    //             MessageBoxA(std::ptr::null_mut(), "Tampering detected\0".as_ptr() as *const _, "echnodrm\0".as_ptr() as *const _, MB_OK);
    //         }
    //     } else {
    //         println!("No debugger found!");
    //     }

        std::thread::sleep_ms(1000);
    }
}
